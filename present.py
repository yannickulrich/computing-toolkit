from tkinter import *
from PIL import Image, ImageTk
from pdf2image import convert_from_path
import player
import subprocess
import time
import os
import yaml


def getwin():
    proc = subprocess.Popen('xwininfo', stdout=subprocess.PIPE)
    stdout = proc.communicate()[0].decode()
    windowId = re.findall('Window id: (0x[\da-f]+)', stdout)[0]
    return windowId


def focus(wid):
    subprocess.Popen(['wmctrl', '-i', '-a', wid])


class main:
    def __init__(self):
        self.demos = {}
        self.load()

        self.tid = getwin()
        self.root = Tk()
        self.root.attributes("-fullscreen", True)
        w, h = self.root.winfo_screenwidth(), self.root.winfo_screenheight()
        self.root.geometry("%dx%d+0+0" % (w, h))
        self.canvas = Canvas(self.root, width=w, height=h)
        self.imagesprite = self.canvas.create_image(w/2, 0, anchor=N)

        h = 930
        w = 4/3*h

        pages = convert_from_path('slides/cc-toolkit.pdf', size=(w, h))
        self.photos = [ImageTk.PhotoImage(i) for i in pages]

        self.canvas.pack()
        self.canvas.configure(background='black')

        self.ind = 0

        self.root.bind('<Left>', self.left)
        self.root.bind('<Right>', self.right)
        self.focus()
        self.root.focus_set()
        self.update()

    def load(self):
        for script in sorted(os.listdir('scripts/')):
            if 'swp' in script:
                continue
            with open('scripts/' + script) as fp:
                content = yaml.safe_load(fp)
            content['script']['done'] = False
            self.demos[content['script']['slide']] = content

    def update(self):
        self.canvas.itemconfig(self.imagesprite, image=self.photos[self.ind])

    def left(self, a):
        self.ind -= 1
        if self.ind < 0:
            self.ind = 0
        self.update()

    def right(self, a):
        self.ind += 1
        if self.ind in self.demos:
            if not self.demos[self.ind]['script']['done']:
                self.demos[self.ind]['script']['done'] = True
                self.unfocus()
                player.play(self.demos[self.ind])
                self.focus()
        if self.ind > len(self.photos)-1:
            self.ind = len(self.photos)-1
        self.update()

    def unfocus(self):
        focus(self.tid)

    def focus(self):
        self.root.focus_force()
        self.root.lift()


if __name__ == '__main__':
    main()
    mainloop()
