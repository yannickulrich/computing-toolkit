# Computing toolkit

This is the first part of what I hope will be a small series of talks
discussing a basic computing toolkit. This series is / was presented
at the IPPP Computing Club at Durham University.

**Warning**: Please only have a look at the final product [here](https://notes.dmaitre.phyip3.dur.ac.uk/computing-club/img/yu-toolkit.pdf).
You should only use this code if you're 100% sure that you know what
you're doing because this code is full of hacks, land mines, dragons,
and dinosaurs with laser guns!
