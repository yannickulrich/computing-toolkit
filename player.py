#!/usr/bin/python
import sys
import yaml
import playitagainsam
import uuid
import tempfile
import os
import six
import time


class myplayer(playitagainsam.Player):
    def _do_read(self, term, recorded):
        if isinstance(recorded, six.text_type):
            recorded = recorded.encode("utf8")
        view_sock = self.terminals[term][0]
        self._do_read_nonwaypoint(view_sock, term, recorded)

    def run(self):
        event = self.eventlog.read_event()
        while event is not None:
            action = event["act"]
            term = event.get("term", None)
            data = event.get("data", None)

            self._maybe_do_live_output(term)

            if action == "OPEN":
                self._do_open_terminal(term)
            elif action == "PAUSE":
                time.sleep(event["duration"])
            elif action == "STOP":
                view_sock = self.terminals[term][0]
                c = view_sock.recv(1)
                while c not in (b'\x1b'):
                    c = view_sock.recv(1)
            elif action == "READ":
                self._do_read(term, data)
            if action == "CLOSE":
                self._do_close_terminal(term)

            event = self.eventlog.read_event()


class mylog(playitagainsam.EventLog):
    def __init__(self, steps, size=[106, 38]):
        self.live_replay = True
        self.shell = '/bin/bash'
        term = uuid.uuid1().hex
        self.terminals = set(term)

        self.events = [{
            "act": "OPEN",
            "size": size,
            "term": term
        }]
        for i in steps:
            if type(i) == str:
                self.events.append({
                    'act': 'ECHO', 'data': i, 'term': term
                })
                self.events.append({
                    'act': 'STOP', 'term': term
                })
                self.events.append({
                    'act': 'READ', 'data': '\r', 'term': term
                })
            elif type(i) == dict:
                if i['act'] == 'PAUSE':
                    name = 'duration'
                else:
                    name = 'data'

                self.events.append({
                    'act': i['act'], name: i['dat'], 'term': term
                })
                self.events.append({
                    'act': 'STOP', 'term': term
                })

        self.events.append({
            'act': 'READ', 'data': '\u0004', 'term': term
        })
        self.events.append({
            'act': 'CLOSE', 'term': term
        })

        self._event_stream = None

    def close(self):
        pass

    def write_event(self):
        pass


def play(fileorcontent):
    if type(fileorcontent) == str:
        with open(fileorcontent) as fp:
            content = yaml.safe_load(fp)
    elif type(fileorcontent) == dict:
        content = fileorcontent

    if 'pre' in content['script']:
        for cmd in content['script']['pre']:
            os.system(cmd)

    currentDir = os.getcwd()
    if 'directory' in content['script']:
        os.chdir(content['script']['directory'])

    eventlog = mylog(content['script']['steps'])

    sockpath = tempfile.NamedTemporaryFile().name
    player = myplayer(
        sockpath,
        eventlog,
        terminal=playitagainsam.util.get_default_terminal(fallback=None),
        auto_type=100,
        auto_waypoint=False,
        live_replay=True,
        replay_shell='/bin/bash'
    )
    player.start()
    playitagainsam.join_player(sockpath)

    if 'post' in content['script']:
        for cmd in content['script']['post']:
            os.system(cmd)

    os.chdir(currentDir)

if __name__ == '__main__':
    play(sys.argv[1])
